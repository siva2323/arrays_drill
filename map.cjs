function map(elements,callBack)
{
    let answer=[];
    if(typeof callBack !== "function" || !Array.isArray(elements)) return elements;
       for(let loop=0;loop<elements.length;loop++)
       {
           answer.push(callBack(elements[loop],loop,elements));
       }
       return answer;
}
module.exports=map;