function each(elements,callBack)
{
        if(typeof callBack !== "function" || !Array.isArray(elements)) return elements;
        for(let loop=0;loop<elements.length;loop++)
        {
            elements[loop]=(callBack(elements[loop],loop,elements));
        }
        return elements;
}
        module.exports=each;
    