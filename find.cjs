function filter(elements,callBack)
{
        let answer=[];
        if(typeof callBack !== "function" || !Array.isArray(elements)) return elements;
       for(let loop=0;loop<elements.length;loop++)
       {
            if(callBack(elements[loop],loop,elements)) 
            {
                answer.push(elements[loop]);
                return answer;
            }
       }

}
module.exports=filter;
