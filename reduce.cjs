
function reduce(elements,callBack,startingValue)
{
    if(typeof callBack !== "function" || !Array.isArray(elements)) return elements;
        let loop=0;
        if( startingValue === null || startingValue === undefined) 
        {
            startingValue=elements[0];
            loop++;
        }
       for(loop;loop<elements.length;loop++)
       {
                startingValue =callBack(startingValue,elements[loop]);
       }
       return startingValue;
}
module.exports=reduce;